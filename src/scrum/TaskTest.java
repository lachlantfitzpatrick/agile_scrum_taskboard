package scrum;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the task class.
 */
public class TaskTest {

    // Series of tasks to be used for testing.
    private Task task1 = new Task("a", "start in ToDo state");
    private Task task2 = new Task("b", "start in InProcess state");
    private Task task3 = new Task("c", "start in ToVerify state");
    private Task task4 = new Task("d", "start in Done state");
    // Series of TaskState instances to represent each state (macros).
    private static Task.TaskState toDo = Task.TaskState.ToDo;
    private static Task.TaskState inProcess = Task.TaskState.InProcess;
    private static Task.TaskState toVerify = Task.TaskState.ToVerify;
    private static Task.TaskState done = Task.TaskState.Done;

    /**
     *
     * Before method that changes the states of the testing tasks.
     *
     */
    @Before
    public void setup(){

        // Reset the states of the testing tasks.
        this.task1 = new Task("a", "ToDo state instance");
        this.task2 = new Task("b", "InProcess state instance");
        this.task3 = new Task("c", "ToVerify state instance");
        this.task4 = new Task("d", "Done state instance");

        // Change the state of task2 to InProcess
        this.task2.changeState("inProcess");

        // Change the state of task3 to ToVerify.
        this.task3.changeState("inProcess");
        this.task3.changeState("toVerify");

        // Change the state of task4 to Done.
        this.task4.changeState("inProcess");
        this.task4.changeState("toVerify");
        this.task4.changeState("done");

    }

    /**
     *
     * Test method for testing the constructor method.
     *
     * Null ID exception tested here.
     *
     */
    @Test
    public void testTask() {

        // Create a task in ToDo state.
        Task task1 = new Task("a", "");

        // Catch the exception thrown when a class with null ID is created.
        try {
            Task task2 = new Task(null, "");
        } catch (IllegalArgumentException illegalArgumentException) {
            Assert.assertEquals("Null ID not allowed.",
                    illegalArgumentException.getMessage());
        }

        // Catch the exception thrown when a task with blank ID is created.
        try {
            Task task3 = new Task("", "");
        } catch (IllegalArgumentException illegalArgumentException) {
            Assert.assertEquals("Blank ID not allowed.",
                    illegalArgumentException.getMessage());
        }

        // Catch the exception thrown when a task with null description is created.
        try {
            Task task4 = new Task("a", null);
        } catch (IllegalArgumentException illegalArgumentException) {
            Assert.assertEquals("Null description not allowed.",
                    illegalArgumentException.getMessage());
        }



        // Create a task in ToVerify state.
        Task task2 = new Task("a", "", "toverify");

        // Catch the exception thrown when a task with null state is created.
        try {
            Task task3 = new Task("a", "", null);
        } catch(IllegalArgumentException illegalArgumentException){
            Assert.assertEquals(illegalArgumentException.getMessage(),
                    "Null state not allowed.");
        }

    }

    /**
     * Test method for testing the clone method.
     */
    @Test
    public void testClone(){

        // Create a task.
        Task task1 = new Task("a", "");
        // Create a second class that clones the first.
        Task task2 = task1.clone();

        // Assert that the IDs and descriptions for the two classes.
        Assert.assertEquals(task1.getDescription(), task2.getDescription());

        // Change the description of the second task.
        task2.setDescription("a");

        // Assert that the descriptions are no longer equal.
        Assert.assertNotEquals(task1.getDescription(), task2.getDescription());

    }

    /**
     * Test method for testing the getID() method.
     */
    @Test
    public void testGetID(){

        // Create a task.
        Task task = new Task("a","");
        // Assert that the task ID is equal to "a".
        Assert.assertEquals("a", task.getID());
        // Assert that the task ID is not equal to "b".
        Assert.assertNotEquals("b", task.getID());

        // Set the task to a new task with symbols for the ID.
        task = new Task("&?|\\n\\t\\r\n`@+1...", "test task");
        // Assert that the task ID is equal to the symbols.
        Assert.assertEquals("&?|\\n\\t\\r\n`@+1...", task.getID());

    }

    /**
     * Test method for testing the getDescription() method.
     */
    @Test
    public void testGetDescription(){

        // Create a task.
        Task task = new Task("a","a");
        // Assert that the task description is equal to "a".
        Assert.assertEquals("a", task.getDescription());
        // Assert that the task ID is not equal to "b".
        Assert.assertNotEquals("b", task.getDescription());

    }

    /**
     * Test method for testing the getState() method.
     */
    @Test
    public void testGetState() {

        // Create a task.
        Task task1 = new Task("test", "A simple test");
        // Assert that the state is equal to "ToDo".
        Assert.assertEquals("ToDo", task1.getState());

    }

    /**
     * Test method for testing the setDescription() method.
     */
    @Test
    public void testSetDescription(){

        // Create a task.
        Task task = new Task("a", "test1");
        // Assert that changing the description to a non null value is
        // successful.
        Assert.assertTrue(task.setDescription("test2"));
        // Confirm that the new task description is "test2".
        Assert.assertEquals("test2", task.getDescription());

        // Catch the exception thrown if trying to set a description to null.
        try {
            task.setDescription(null);
        } catch(IllegalArgumentException illegalArgumentException){
            Assert.assertEquals("Null description not allowed.",
                    illegalArgumentException.getMessage());
        }

    }

    /*
    THE FOLLOWING CODE IS FOR TESTING CHANGES BETWEEN ALL THE STATES HENCE THE
    MESSY NATURE OF IT...
     */


    /**
     * Test method for testing the task class changeState method.
     *
     * Tests all illegal state changes followed by all legal state changes.
     * Illegal comes first as they do not modify the task state whereas the
     * legal state changes do.
     */
    @Test
    public void testChangeStates(){

        // Test all the illegal cases.
        // Test all the illegal cases when starting in the Done state.
        // Initialise a task in the Done state.
        Task task = this.task4;
        // Assert that changing the state from Done to ToDo is unsuccessful.
        Assert.assertFalse(changeState(task, "toDo"));
        // Assert that changing the state from Done to InProcess is
        // unsuccessful.
        Assert.assertFalse(changeState(task, "inProcess"));
        // Assert that changing the state from Done to ToVerify is
        // unsuccessful.
        Assert.assertFalse(changeState(task, "toVerify"));
        // Assert that changing the state from Done to Done is unsuccessful.
        Assert.assertFalse(changeState(task, "done"));
        // Test all the illegal cases when starting in the ToVerify state.
        // Set the task to a task in the ToVerify state.
        task = this.task3;
        // Assert that changing the state from ToVerify to ToVerify is
        // unsuccessful.
        Assert.assertFalse(changeState(task, "toVerify"));
        // Test all the illegal cases when starting in the InProcess state.
        // Set the task to a task in the InProcess state.
        task = this.task2;
        // Assert that changing the state from InProcess to Done is
        // unsuccessful.
        Assert.assertFalse(changeState(task, "done"));
        // Set the task to a task in the ToDo state.
        task = this.task1;
        // Assert that changing the state from ToDo to Done is unsuccessful.
        Assert.assertFalse(changeState(task, "done"));
        // Assert that changing the state from ToDo to ToVerify is
        // unsuccessful.
        Assert.assertFalse(changeState(task, "toVerify"));

        // Test all the legal cases.
        // Set task to a task in the ToDo state.
        task = this.task1;
        // Assert that changing the state from ToDo to ToDo is successful.
        Assert.assertTrue(changeState(task, "toDo"));
        // Assert that changing the state from ToDo to InProcess is
        // successful.
        Assert.assertTrue(changeState(task, "inProcess"));
        // Assert that changing the state from InProcess to InProcess is
        // successful.
        Assert.assertTrue(changeState(task, "inProcess"));
        // Assert that changing the state from InProcess to ToDo is
        // successful.
        Assert.assertTrue(changeState(task, "toDo"));
        // Change the state back to InProcess.
        changeState(task, "inProcess");
        // Assert that changing the state from InProcess to ToVerify is
        // successful.
        Assert.assertTrue(changeState(task, "toVerify"));
        // Assert that changing the state from ToVerify to ToDo is successful.
        Assert.assertTrue(changeState(task, "toDo"));
        // Set the task to a task in the ToVerify state.
        task = this.task3;
        // Assert that changing the state from ToVerify to InProcess is
        // successful.
        Assert.assertTrue(changeState(task, "inProcess"));
        // Change the state back to ToVerify.
        changeState(task, "toVerify");
        // Assert that changing the state from ToVerify to Done is successful.
        Assert.assertTrue(changeState(task, "done"));

    }

    /**
     * Test method for testing the checkStateLegality() method.
     *
     * Tests all illegal state changes followed by all legal state changes.
     * Illegal comes first as they do not modify the task state whereas the
     * legal state changes do.
     */
    @Test
    public void testCheckStateChangeLegality(){

        // Test all the illegal cases.
        // Test all the illegal cases when starting in the Done state.
        // Initialise a task in the Done state.
        Task task = this.task4;
        // Assert that changing the state from Done to ToDo is unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, toDo));
        // Assert that changing the state from Done to InProcess is
        // unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, inProcess));
        // Assert that changing the state from Done to ToVerify is
        // unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, toVerify));
        // Assert that changing the state from Done to Done is unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, done));
        // Test all the illegal cases when starting in the ToVerify state.
        // Set the task to a task in the ToVerify state.
        task = this.task3;
        // Assert that changing the state from ToVerify to ToVerify is
        // unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, toVerify));
        // Test all the illegal cases when starting in the InProcess state.
        // Set the task to a task in the InProcess state.
        task = this.task2;
        // Assert that changing the state from InProcess to Done is
        // unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, done));
        // Set the task to a task in the ToDo state.
        task = this.task1;
        // Assert that changing the state from ToDo to Done is unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, done));
        // Assert that changing the state from ToDo to ToVerify is
        // unsuccessful.
        Assert.assertFalse(checkChangeStateLegality(task, toVerify));

        // Test all the legal cases.
        // Set task to a task in the ToDo state.
        task = this.task1;
        // Assert that changing the state from ToDo to ToDo is successful.
        Assert.assertTrue(checkChangeStateLegality(task, toDo));
        // Assert that changing the state from ToDo to InProcess is
        // successful.
        Assert.assertTrue(checkChangeStateLegality(task, inProcess));
        // Set task to a task in the InProcess state.
        task = this.task2;
        // Assert that changing the state from InProcess to ToDo is
        // successful.
        Assert.assertTrue(checkChangeStateLegality(task, toDo));
        // Assert that changing the state from InProcess to InProcess is
        // successful.
        Assert.assertTrue(checkChangeStateLegality(task, inProcess));
        // Assert that changing the state from InProcess to ToVerify is
        // successful.
        Assert.assertTrue(checkChangeStateLegality(task, toVerify));
        // Set task to a task in the ToVerify state.
        task = this.task3;
        // Assert that changing the state from ToVerify to ToDo is successful.
        Assert.assertTrue(checkChangeStateLegality(task, toDo));
        // Assert that changing the state from ToVerify to InProcess is
        // successful.
        Assert.assertTrue(checkChangeStateLegality(task, inProcess));
        // Assert that changing the state from ToVerify to Done is successful.
        Assert.assertTrue(checkChangeStateLegality(task, done));

    }

    /**
     * Method that is used to try and change the state of a task and return
     * true or false with no exceptions thrown for the convenience of testing.
     *
     * @param task
     *      The task of interest.
     *
     * @param taskState
     *      The state that the task will be attempted to change into.
     *
     * @return
     *      A boolean indicating whether the change was successful or the
     *      illegal state change exception was thrown.
     */
    public boolean changeState(Task task, String taskState){

        // Try to change the state of the task.
        try{
            // Change the state of the task and return the result.
            return task.changeState(taskState);

        } catch(IllegalStateChangeException ex){

            // Return false if the illegal state change exception was thrown.
            return false;

        }
    }

    /**
     * Method that is used to check the legality of a task state change and
     * return true or false with no exceptions thrown for the convenience of
     * testing.
     *
     * @param task
     *      The task of interest.
     *
     * @param taskState
     *      The state that the change legality will be tested for.
     *
     * @return
     *      A boolean indicating whether the change is legal or not.
     */
    public boolean checkChangeStateLegality(Task task, Task.TaskState taskState){

        // Try to change the state of the task.
        try{
            // Change the state of the task and return the result.
            return task.checkStateChangeLegality(taskState);

        } catch(IllegalStateChangeException ex){

            // Return false if the illegal state change exception was thrown.
            return false;

        }
    }

    /**
     * Method to test the stringToTaskState method.
     */
    @Test
    public void testStringToTaskState(){

        // Create a board.
        Task task = new Task("a","a");

        // Assert that several different string representations of all the
        // states successfully return the expected state.
        junit.framework.Assert.assertEquals(Task.TaskState.ToDo, task.stringToTaskState("todo"));
        junit.framework.Assert.assertEquals(Task.TaskState.ToDo, task.stringToTaskState("toDo"));
        junit.framework.Assert.assertEquals(Task.TaskState.ToDo, task.stringToTaskState("t  O D    o"));
        junit.framework.Assert.assertEquals(Task.TaskState.Done, task.stringToTaskState("D O ne"));

        // Assert that passing a string that does not represent a column throws
        // an illegal state exception.
        try{
            task.stringToTaskState("test");
        } catch(IllegalStateException illegalStateException){
            junit.framework.Assert.assertEquals("Invalid column.",
                    illegalStateException.getMessage());
        }

    }
}