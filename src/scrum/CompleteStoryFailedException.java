package scrum;

/**
 * A custom exception to handle a failed attempt to declare a story complete.
 *
 * Created by Lachlan.
 */
public class CompleteStoryFailedException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public CompleteStoryFailedException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public CompleteStoryFailedException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
