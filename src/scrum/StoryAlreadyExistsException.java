package scrum;

/**
 * A custom exception to handle a duplicate task ID.
 *
 * Created by Lachlan.
 */
public class StoryAlreadyExistsException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public StoryAlreadyExistsException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public StoryAlreadyExistsException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
