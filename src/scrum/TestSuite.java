package scrum;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite for the scrum project.
 * Created by Lachlan.
 */

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TaskTest.class,
        StoryTest.class,
        ScrumBoardTest.class,
        ScrumIOTest.class,
        ControllerTest.class
})
public class TestSuite {

}
