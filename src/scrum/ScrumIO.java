package scrum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This class handles the IO to and from the controller.
 *
 * Created by Lachlan.
 */
public class ScrumIO {

    // Array of all the commands.
    private Map<String, Integer> commandsMap;

    /**
     * The constructor method.
     */
    public ScrumIO(){

        // Initialise the commands map.
        commandsMap = new HashMap<String, Integer>();
        populateCommandsMap();

    }

    /**
     * Method that reads input and splits it into commands.
     *
     * @throws IllegalCommandStructureException
     *      Throws the IllegalCommandStructureException from formatInput method.
     *
     * @return
     *      An array holding each element of the input.
     */
    public ArrayList<String> readInput()
            throws IllegalCommandStructureException{

        // Array of string to hold the formatted input.
        ArrayList<String> formattedInput;

        // Create a scanner to handle the input.
        Scanner scanInput = new Scanner(System.in);

        // Try to read the next line of input.
        try {

            // Initialise the string to hold the input.
            String input = "";
            // Initialise the boolean determine when valid input was received.
            boolean receivedInput = false;

            // Wait for input.
            while(!receivedInput){

                // If there is input on the next line and the input is not
                // an empty line exit the loop.
                if(scanInput.hasNextLine()){

                    // Get the input on the next line.
                    input = scanInput.nextLine();

                    // If the input is not empty exit the loop.
                    if(!input.isEmpty()){

                        // Exit the loop.
                        receivedInput = true;

                    }

                }

            }

            // Return an array of formatted strings.
            formattedInput = formatInput(input);
            return formattedInput;

        }
        // Catch the illegal command structure exception and throw it.
        catch(IllegalCommandStructureException illegalCommandStructureException){

            // Throw illegal command structure exception.
            throw  illegalCommandStructureException;

        }
        // Catch both the NoSuchElementException and IllegalStateException
        // thrown by the nextLine method. Both of these should exit the
        // program as they should not occur.
        catch(RuntimeException runtimeException){

            // Print the exception message.
            System.out.println(runtimeException.getMessage());

            // Close the scanner.
            scanInput.close();

            // Close the program.
            System.exit(1);

            // Return null.
            return null;

        }

    }

    /**
     * Method that takes an array of string arrays and outputs it.
     *
     * @param stringArrays
     *      Write the output of string arrays to the IO stream.
     *
     * @return
     *      Return true if successful.
     */
    public boolean writeOutput(ArrayList<ArrayList<String>> stringArrays){

        // Try to output each element of stringArray on a new line. Catch the
        // null pointer exception when it is thrown and just return false. It
        // is ok to not output anything when null is passed in.
        try {

            // Output each element of stringArrays on a new line.
            for (ArrayList<String> stringArray : stringArrays) {

                // Format the string array then output it.
                System.out.println(formatOutput(stringArray));

            }

            // Return true.
            return true;

        } catch(NullPointerException nullPointerException){

            // Print the null point exception.
            System.out.println(nullPointerException.getMessage());

            // Return false.
            return false;

        }

    }

    /**
     * Method that formats an inputted string.
     *
     * @param input
     *      The string input.
     *
     * @return
     *      The array list that represents the inputted string in the format to
     *      use in the controller.
     */
    public ArrayList<String> formatInput(String input){

        // Initialise the formatted components string.
        ArrayList<String> formattedComponents = new ArrayList<String>();

        // The first two components of the command are the command itself so get these.
        String[] inputArray = input.split("\\s+", 3);
        String command = inputArray[0];
        // If the input command is longer than two components get the second component.
        if(inputArray.length > 1){

            // Add the first element of the formatted input and second element
            // of input together.
            command += " " + inputArray[1];

        }

        // Add the command as a component.
        formattedComponents.add(command);

        // If all the inputted command has been processed return the
        // formattedComponents now.
        if(inputArray.length < 3){

            // Return the formatted components.
            return formattedComponents;

        }

        // Get the third element in input array and split it to get the number
        // of arguments.
        String[] arguments = inputArray[2].split("\\s+",
                commandsMap.get(formattedComponents.get(0)));

        // Split the input string into its elements.
        for(String element : arguments){

            // Add the element to the output.
            formattedComponents.add(element);

        }


        // Return the formatted array of strings.
        return formattedComponents;

    }

    /**
     * Method that formats the output.
     *
     * @param stringArray
     *      The array of strings to be formatted for output.
     *
     * @throws  NullPointerException
     *      Throws a NullPointerException.
     *
     * @return
     *      Returns the string formatted for output.
     */
    public String formatOutput(ArrayList<String> stringArray)
            throws NullPointerException{

        // Initialise the string that holds the output.
        String output = "";

        // Add each element of the string array to the output string.
        for(int i = 0; i < stringArray.size(); i++){

            // If it is not the last element add the element of string array
            // plus a dash to the output string.
            if(i != stringArray.size() - 1){

                // Add the element of string array plus a dash to the output
                // string.
                output += stringArray.get(i) + " - ";

            } else{

                // Just add the element of string array.
                output += stringArray.get(i);

            }

        }

        // Return the formatted string.
        return output;

    }

    /**
     * Method that takes two string and returns the number of occurrences of
     * the second string in the first.
     *
     * @param string
     *      The string with occurrences in it.
     *
     * @param occurrence
     *      The occurrence to be checked for in string.
     *
     * @return
     *      Returns the integer number of occurences in string.
     */
    public int countOccurrences(String string, String occurrence){

        // Return the difference in the length of the string after removing the
        // occurrences from it and dividing by the length of the occurrence.
        return (int)(string.length() - string.replace(occurrence, "").length())
                /occurrence.length();

    }

    /**
     * Method that populates the commands map.
     */
    public void populateCommandsMap(){

        // Add all the commands and the number of arguments they take to the
        // commands map.
        commandsMap.put("create story", 2);
        commandsMap.put("list stories", 0);
        commandsMap.put("delete story", 1);
        commandsMap.put("complete story", 1);
        commandsMap.put("create task", 3);
        commandsMap.put("list tasks", 1);
        commandsMap.put("delete task", 2);
        commandsMap.put("move task", 3);
        commandsMap.put("update task", 3);

    }

}
