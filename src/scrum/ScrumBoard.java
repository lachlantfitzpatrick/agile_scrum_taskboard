package scrum;

import java.util.ArrayList;

/**
 * This class handles the interaction between the board items and the
 * controller.
 *
 * Created by Lachlan.
 */
public class ScrumBoard {

    // ArrayList to hold the stories that are on this board.
    private ArrayList<Story> stories;

    /**
     * The constructor method. All boards start empty.
     */
    public ScrumBoard(){

        // Initialise stories.
        stories = new ArrayList<Story>();

    }

    /**
     * Method that loads the state of the program based off a state. The state
     * is represented by arrays of string arrays representing stories and
     * tasks.
     *
     * @param state
     *      The representation of the state.
     *
     * @throws StoryAlreadyCompleteException
     *      Thrown from the createStory method.
     *
     * @throws StoryDoesNotExistException
     *      Thrown from the completeStory and createTask methods.
     *
     * @throws IllegalArgumentException
     *      Thrown from the createStory and createTask methods.
     *
     * @throws TaskAlreadyExistsException
     *      Thrown from the createTask method.
     *
     * @throws IllegalStateException
     *      Thrown from createTask method.
     *
     * @return
     *      Returns true for successful completion of the method.
     */
    public boolean loadState(ArrayList<ArrayList<String>> state)
            throws StoryAlreadyExistsException, StoryDoesNotExistException,
            IllegalArgumentException, TaskAlreadyExistsException,
            IllegalStateException{

        // Load all the stories first.
        for(ArrayList<String> boardItem : state){

            // Check whether is is a story.
            if(boardItem.size() == 3){

                // Create the story.
                createStory(boardItem.get(0), boardItem.get(2));

                // If the story was completed, complete it.
                if(boardItem.get(1).equals("complete")){

                    // Complete the story.
                    completeStory(boardItem.get(0));

                }

            }

        }

        // Load all the tasks second.
        for(ArrayList<String> boardItem : state){

            // Check whether is is a story.
            if(boardItem.size() == 4){

                // Create the task.
                createTask(boardItem.get(3), boardItem.get(0),
                        boardItem.get(2), boardItem.get(1));

            }

        }

        return true;

    }

    /**
     * Adder method for adding a new story to stories (creating a story).
     *
     * @param storyID
     *      The ID of the story to be added to stories.
     *
     * @param storyDescription
     *      The description of the story to be added to stories.
     *
     * @throws IllegalArgumentException
     *      Throws the illegal argument exception from the Story constructor.
     *
     * @throws StoryAlreadyExistsException
     *      Throws the task already exists exception if the story already
     *      exists.
     *
     * @return
     *      Return true if the story was added successfully.
     */
    public boolean createStory(String storyID, String storyDescription)
            throws IllegalArgumentException, StoryAlreadyExistsException {

        // Check whether the story exists.
        if(storyExists(storyID)){

            // Throw the story already exists exception.
            throw new StoryAlreadyExistsException("Story already exists.");

        }

        // Create the new story.
        Story story = new Story(storyID, storyDescription);
        // Add the new story to stories.
        stories.add(story);
        // Return true.
        return true;

    }

    /**
     * Getter method to return an array of string arrays of all the stories.
     *
     * @return
     *      Return an array of string representations of the story.
     */
    public ArrayList<ArrayList<String>> listStories(){

        // Initiate the array list to hold the array lists.
        ArrayList<ArrayList<String>> storiesList = new ArrayList<ArrayList<String>>();

        // For every story, get the key information and store it in a string
        // array then store the string array in items.
        for(Story story : stories){

            // Instantiate a new string array.
            ArrayList<String> stringArray = new ArrayList<String>();

            // Add all the key information.
            stringArray.add(story.getID());
            stringArray.add(story.getState());
            stringArray.add(story.getDescription());

            // Add the string array to stringArrays.
            storiesList.add(stringArray);

        }

        // Return string arrays.
        return storiesList;

    }

    /**
     * Method that deletes a story with a given ID.
     *
     * @param storyID
     *      Takes the story ID that identifies the story to be deleted.
     *
     * @throws StoryDoesNotExistException
     *      Throws a story does not exist exception when the story to be deleted
     *      does not exist.
     *
     * @return
     *      Return true if the story was deleted successfully.
     */
    public boolean deleteStory(String storyID) throws StoryDoesNotExistException{

        // Remove the story.
        stories.remove(getStoryIndex(storyID));

        // Return true.
        return true;

    }

    /**
     * Method to try and complete the story.
     *
     * @param storyID
     *
     * @throws CompleteStoryFailedException
     *      Throws the CompleteStoryFailedException from completeStory method.
     *
     * @throws StoryAlreadyCompleteException
     *      Throws the StoryAlreadyCompleteException from completeStory method.
     *
     * @return
     *      Return true if the method was successful.
     */
    public boolean completeStory(String storyID)
            throws CompleteStoryFailedException, StoryAlreadyCompleteException{

        // Set the story state.
        return stories.get(getStoryIndex(storyID)).completeStory();

    }

    /**
     * Adder method to create a task in a story.
     *
     * @param storyID
     *      The ID of the story.
     *
     * @param taskID
     *      The ID of the task.
     *
     * @param taskDescription
     *      The description of the task.
     *
     * @throws StoryDoesNotExistException
     *      Throws the StoryDoesNotExistException exception from getStoryIndex.
     *
     * @throws IllegalArgumentException
     *      Throws the IllegalArgumentException from createTask.
     *
     * @throws TaskAlreadyExistsException
     *      Throws the TaskAlreadyExistsException from createTask.
     */
    public boolean createTask(String storyID, String taskID,
                           String taskDescription)
            throws StoryDoesNotExistException, IllegalArgumentException,
            TaskAlreadyExistsException{

        // Create the task in the story.
        stories.get(getStoryIndex(storyID)).createTask(taskID, taskDescription);

        // Return true.
        return true;
    }

    /**
     * Adder method to create a task in a story.
     *
     * @param taskID
     *      The ID of the story.
     *
     * @param taskDescription
     *      The description of the task.
     *
     * @param state
     *      The state the task will be created in.
     *
     * @throws StoryDoesNotExistException
     *      Throws the StoryDoesNotExistException exception from getStoryIndex.
     *
     * @throws IllegalArgumentException
     *      Throws the IllegalArgumentException from createTask.
     *
     * @throws TaskAlreadyExistsException
     *      Throws the TaskAlreadyExistsException from createTask.
     */
    public boolean createTask(String storyID, String taskID,
                              String taskDescription, String state)
            throws StoryDoesNotExistException, IllegalArgumentException,
            TaskAlreadyExistsException, IllegalStateException{

        // Create the task in the story with given state.
        stories.get(getStoryIndex(storyID))
                .createTask(taskID, taskDescription, state);

        // Return true.
        return true;
    }

    /**
     * Getter method to return a string array of all the tasks in a story.
     *
     * @param storyID
     *      The ID of the story to list the tasks of.
     *
     * @throws StoryDoesNotExistException
     *      Throws the StoryDoesNotExistException from getDtoryIndex.
     *
     * @return
     *      An array list of array lists describing the tasks in the story.
     */
    public ArrayList<ArrayList<String>> listTasks(String storyID)
            throws StoryDoesNotExistException {

        // Initiate the array list to hold the array lists.
        ArrayList<ArrayList<String>> tasksList = new ArrayList<ArrayList<String>>();

        // Make a copy of the tasks array in the given story.
        ArrayList<Task> tasks = stories.get(getStoryIndex(storyID)).getTasks();

        // For every task, get the key information and store it in a string
        // array then store the string array in items.
        for (Task task : tasks) {

            // Instantiate a new string array.
            ArrayList<String> stringArray = new ArrayList<String>();

            // Add all the key information.
            stringArray.add(task.getID());
            stringArray.add(task.getState());
            stringArray.add(task.getDescription());

            // Add the string array to the tasks list.
            tasksList.add(stringArray);

        }

        // Return string arrays.
        return tasksList;
    }

    /**
     * Method that deletes a task in a story.
     *
     * @param storyID
     *      The Id of the story.
     *
     * @param taskID
     *      The Id of the task.
     *
     * @throws StoryDoesNotExistException
     *      Throw the StoryDoesNotExistException from getStoryIndex.
     *
     * @throws TaskDoesNotExistException
     *      Throws the TaskDoesNotExistException from deleteTask.
     *
     * @return
     *      Return true for successful completion.
     */
    public boolean deleteTask(String storyID, String taskID)
            throws StoryDoesNotExistException, TaskDoesNotExistException{

        // Create the task in the story.
        stories.get(getStoryIndex(storyID)).deleteTask(taskID);

        // Return true.
        return true;
    }

    /**
     * Method that moves a task in a story to a new state.
     *
     * @param storyID
     *      The ID of the story that the task will be created in.
     *
     * @param taskID
     *      The ID of the task.
     *
     * @param state
     *      The state the task will be created in.
     *
     * @throws StoryDoesNotExistException
     *      Throws the StoryDoesNotExistException exception from getStoryIndex.
     *
     * @throws IllegalArgumentException
     *      Throws the IllegalArgumentException from moveTask.
     *
     * @throws TaskDoesNotExistException
     *      Throws the TaskDoesNotExistException from moveTask.
     *
     * @throws IllegalStateException
     *      Throws the IllegalStateException from moveTask.
     *
     * @return
     *      Return true for successful completion.
     */
    public boolean moveTask(String storyID, String taskID, String state)
            throws TaskDoesNotExistException, IllegalStateChangeException,
            IllegalStateException, StoryDoesNotExistException{

        // Create the task in the story.
        stories.get(getStoryIndex(storyID)).moveTask(taskID, state);

        // Return true.
        return true;
    }

    /**
     * Method that updates a task in a story with a new description.
     *
     * @param storyID
     *      The ID of the story that the task will be created in.
     *
     * @param taskID
     *      The ID of the task.
     *
     * @param description
     *      The stanew description of the task.
     *
     * @throws StoryDoesNotExistException
     *      Throws the StoryDoesNotExistException exception from getStoryIndex.
     *
     * @throws IllegalArgumentException
     *      Throws the IllegalArgumentException from updateTask.
     *
     * @throws TaskDoesNotExistException
     *      Throws the TaskDoesNotExistException from updateTask.
     *
     * @return
     *      Return true for successful completion.
     */
    public boolean updateTask(String storyID, String taskID, String description)
            throws TaskDoesNotExistException, StoryDoesNotExistException,
            IllegalArgumentException{

        // Create the task in the story.
        stories.get(getStoryIndex(storyID)).updateTask(taskID, description);

        // Return true.
        return true;
    }

    /**
     * Method that lists every task on the board with the story ID included
     * in the task's characteristics.
     *
     * @return
     *      List all the tasks currently on the board.
     */
    public ArrayList<ArrayList<String>> listAllTasks(){

        // Initiate the array list to hold the array lists.
        ArrayList<ArrayList<String>> masterTasksList =
                new ArrayList<ArrayList<String>>();

        // For every story, get the tasks list and add each task in that
        // list to the master tasks list with the story ID added to the task
        // characteristics.
        for (Story story : stories) {

            // Initiate the array list to hold the array lists.
            ArrayList<ArrayList<String>> tasksList = listTasks(story.getID());

            // Add each task in the list to the master list with the story ID
            // added to the task characteristics.
            for(ArrayList<String> task : tasksList){

                // Add the story ID.
                task.add(story.getID());

                // Add the task to the master list.
                masterTasksList.add(task);

            }

        }

        // Return string arrays.
        return masterTasksList;
    }

    /**
     * Getter method that returns whether a story exists or not in stories.
     *
     * @param storyID
     *      The ID of the story to be tested for existence.
     *
     * @return
     *      Return true if the story exists or false otherwise.
     */
    public boolean storyExists(String storyID){

        // Find the story with the given ID.
        for(Story story : stories){

            // Check for matching ID.
            if(story.getID().equals(storyID)){

                // Return true for match.
                return true;

            }

        }

        // Return false for no match.
        return false;

    }

    /**
     * Getter method that returns the index of a story in stories.
     *
     */
    public int getStoryIndex(String storyID) throws StoryDoesNotExistException{

        // Find the story with the given ID.
        for(int i = 0; i < stories.size(); i++){

            // Check for matching ID.
            if(stories.get(i).getID().equals(storyID)){

                // Return index.
                return i;

            }

        }

        // No match was found, throws the story does not exist exception.
        throw new StoryDoesNotExistException("Story ID does not exist.");

    }

}
