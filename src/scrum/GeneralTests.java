package scrum;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Class that is used to run general tests on code ideas.
 *
 * Created by Lachlan on 30/03/2015.
 */
public class GeneralTests {

    /**
     * Test method for testing copies of ArrayList<> to prevent breaking the
     * invariant.
     */
    @Test
    public void testArrayList(){

        // Create a couple of tasks for testing.
        Task task1 = new Task("a", "a");
        Task task2 = new Task("b", "b");
        Task task3 = new Task("c", "c");
        // Create a new ArrayList of tasks.
        ArrayList<Task> tasks1  = new ArrayList<Task>();
        tasks1.add(task1);
        tasks1.add(task2);

        // Make a deep copy of the array list.
        ArrayList<Task> tasks2 = new ArrayList<Task>();
        for(Task t : tasks1){

            // Add a clone of each copy to tasks2.
            tasks2.add(t.clone());

        }

        // Assert that the first elements of the two Arraylists are the same.
        Assert.assertEquals(tasks1.get(1).getDescription(),
                tasks2.get(1).getDescription());

        // Change the description of the first element in tasks2.
        tasks2.get(1).setDescription("c");
        // Assert that the description of the first element in both array lists
        // is not equal.
        Assert.assertNotEquals(tasks2.get(1).getDescription(),
                tasks1.get(1).getDescription());

    }

    /**
     * Method to test whether assigning one instance of enum to another clones
     * the original enum or just points to the same object. This was important
     * for preventing a breach of the invariant in the changeState() method
     * when the proposed task state is assigned to the internal task state.
     */
    @Test
    public void testEnum(){

        // Check that two task state instances set to the same state are equal.
        Task.TaskState test1 = Task.TaskState.ToDo;
        Task.TaskState test2 = Task.TaskState.ToDo;
        Assert.assertEquals(test1, test2);

        // Check that a new task state assigned to an existing task state is
        // equal to the existing task state.
        Task.TaskState test3 = test2;
        Assert.assertEquals(test3, test1);
        // Check that assigning the new task state to a another new task state
        // does NOT set the original task state to the new task state (ie. test
        // whether enum objects are pointers to the data).
        test3 = Task.TaskState.InProcess;
        Assert.assertNotEquals(test3, test2);

    }
}
