package scrum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Class the handles the interaction between the board and user.
 *
 * Created by Lachlan.
 */
public class Controller {

    // Instantiate an instance of the IO handler.
    private ScrumIO scrumIO;
    // Instantiate an instance of the scrum board.
    private ScrumBoard board;
    // Instantiate an instance of persistence.
    private Persistence persistence;
    // Array of all the commands.
    private Map<String, Integer> commandsMap;



    /**
     * The constructor method.
     */
    public Controller(){

        // Initialise the IO handler.
        scrumIO = new ScrumIO();
        // Initialise the scrum board.
        board = new ScrumBoard();
        // Initialise persistence.
        persistence = new Persistence();
        // Initialise the commands map.
        commandsMap = new HashMap<String, Integer>();
        populateCommandsMap();

    }

    /**
     * Method that requests the IO handler to read input, pass that input off
     * to the board then pass any output back to the IO handler.
     *
     * Catch all exceptions here except IO exceptions (they will be handled in
     * the IO handler). All exceptions should now pass the message to be
     * outputted.
     *
     * @return
     *      Return true if the method is executed successfully.
     */
    public boolean handleIO(){

    // Try to read the input and use it.
    try {
        // Read the input.
        ArrayList<String> readInput = scrumIO.readInput();

        // Decide which method to use.
        if(readInput == null){

            return false;

        } else if(readInput.get(0).equals("save")){

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        } else if(readInput.get(0).equals("load")){

            // Call load program method.
            loadProgramState();

            // Return true.
            return true;

        } else if(readInput.get(0).equals("exit")){

            // Call save program method.
            saveProgramState();

            // Call create story method.
            System.exit(0);

            // Return true.
            return true;

        } else if(readInput.get(0).equals("create story")){

            // Call create story method.
            createStory(readInput);

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        } else if(readInput.get(0).equals("list stories")){

            // Call list stories method.
            listStories(readInput);

            // Return true.
            return true;

        } else if(readInput.get(0).equals("delete story")){

            // Call delete story method.
            deleteStory(readInput);

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        } else if(readInput.get(0).equals("complete story")){

            // Call complete story method.
            completeStory(readInput);

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        } else if(readInput.get(0).equals("create task")){

            // Call create task method.
            createTask(readInput);

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        } else if(readInput.get(0).equals("list tasks")){

            // Call list task method.
            listTasks(readInput);

            // Return true.
            return true;

        } else if(readInput.get(0).equals("delete task")){

            // Call delete task method.
            deleteTask(readInput);

            // Call save program method.
            saveProgramState();

            return true;

        } else if(readInput.get(0).equals("move task")){

            // Call move task method.
            moveTask(readInput);

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        }  else if(readInput.get(0).equals("update task")){

            // Call update task method.
            updateTask(readInput);

            // Call save program method.
            saveProgramState();

            // Return true.
            return true;

        }

        // The given command was not found. Print a message to the user.
        scrumIO.writeOutput(
                singleEntryInNestedArray("Command not found."));

        // Return false.
        return false;

    } catch(IllegalCommandStructureException illegalCommandStructureException){

        // The given command structure was incorrect. Print a message to
        // the user.
        scrumIO.writeOutput(singleEntryInNestedArray(
                illegalCommandStructureException.getMessage()));

        // Return false.
        return false;

    }

    }

    /**
     * Method that handles saving the program state.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean saveProgramState(){

        // Try to save the program state.
        try{

            // Save the program state.
            persistence.saveProgramState(board.listStories(),
                    board.listAllTasks());

            // Return true.
            return true;

        } catch(IOException IOException){

            // Something went wrong in the IO, print the stack trace.
            IOException.printStackTrace();

            // Return false
            return false;

        }

    }

    /**
     * Method tha handles loading a program state.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean loadProgramState(){

        // Try to load the program state.
        try{

            // Load the program state.
            board.loadState(persistence.loadProgramState());

            return true;

        } catch(InvalidPersistenceFileException invalidPersistenceFileException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(
                    invalidPersistenceFileException.getMessage()));

            return false;

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(
                    runtimeException.getMessage()));

            return false;

        } catch(IOException IOException){

            // Something went wrong in the IO, print the stack trace.
            scrumIO.writeOutput(singleEntryInNestedArray(
                    "System IO error occurred."));

            // Return false
            return false;

        }

    }

    /**
     * Method that handles creating a story.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean createStory(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to create the story.
        try{

            // Create the story.
            board.createStory(command.get(1),command.get(2));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles listing the stories.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean listStories(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to create the story and catch and print the message of all
        // exception.
        try{

            // Output the stories.
            scrumIO.writeOutput(board.listStories());

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles deleting stories.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean deleteStory(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to create the story and catch and print the message of all
        // exception.
        try{

            // Delete the story.
            board.deleteStory(command.get(1));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles completing stories.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean completeStory(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to create the story and catch and print the message of all
        // exception.
        try{

            // Complete the story.
            board.completeStory(command.get(1));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles creating a task in a story.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean createTask(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to create the story.
        try{

            // Create the story.
            board.createTask(command.get(1), command.get(2), command.get(3));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles listing the tasks.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean listTasks(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to list the tasks and catch and print the message of all
        // exceptions.
        try{

            // Output the stories.
            scrumIO.writeOutput(board.listTasks(command.get(1)));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles deleting tasks.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean deleteTask(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to delete the task and catch and print the message of all
        // exceptions.
        try{

            // Output the stories.
            board.deleteTask(command.get(1), command.get(2));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles moving a task in a story.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean moveTask(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to move the task.
        try{

            // Move the task.
            board.moveTask(command.get(1), command.get(2), command.get(3));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that handles updating a task in a story.
     *
     * @return
     *      Returns true if the method wsa successful.
     */
    public boolean updateTask(ArrayList<String> command){

        // Check that the correct number of arguments were supplied.
        if(command.size() - 1 != commandsMap.get(command.get(0))){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray("Incorrect number of " +
                    "arguments for command."));

            // Return false.
            return false;


        }

        // Try to update the task.
        try{

            // Update the task.
            board.updateTask(command.get(1), command.get(2), command.get(3));

        } catch(RuntimeException runtimeException){

            // Print a message to the user.
            scrumIO.writeOutput(singleEntryInNestedArray(runtimeException.getMessage()));

            return false;
        }

        // Return true.
        return true;

    }

    /**
     * Method that puts a single string in the data structure
     * ArrayList<ArrayList<String>> so that a single string can be printed.
     *
     * @param entry
     *
     * @return
     *      Return the string nested in an the array that the output function
     *      takes.
     */
    public ArrayList<ArrayList<String>> singleEntryInNestedArray(String entry){

        // Initialise a string array and an array of string arrays.
        ArrayList<ArrayList<String>> stringArrays = new ArrayList<ArrayList<String>>();
        ArrayList<String> stringArray = new ArrayList<String>();

        // Add the string to the string array and the string array to the
        // array of string arrays.
        stringArray.add(entry);
        stringArrays.add(stringArray);

        // Return the array of string arrays.
        return stringArrays;

    }

    /**
     * Method that populates the commands map.
     */
    public void populateCommandsMap(){

        // Add all the commands and the number of arguments they take to the
        // commands map.
        commandsMap.put("create story", 2);
        commandsMap.put("list stories", 0);
        commandsMap.put("delete story", 1);
        commandsMap.put("complete story", 1);
        commandsMap.put("create task", 3);
        commandsMap.put("list tasks", 1);
        commandsMap.put("delete task", 2);
        commandsMap.put("move task", 3);
        commandsMap.put("update task", 3);

    }
}
