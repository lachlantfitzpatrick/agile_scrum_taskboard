package scrum;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test class for the ScrumBoard class.
 */
public class ScrumBoardTest {

    /**
     * Method to test the createStory method.
     */
    @Test
    public void testCreateStory(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Assert that creating a story in board is successful.
        Assert.assertTrue(board.createStory("a", ""));

        // Assert that creating a story with blank ID throws an illegal
        // argument exception.
        try{
            board.createStory("","");
        } catch(IllegalArgumentException illegalArgumentException){
            Assert.assertEquals("Blank ID not allowed.",
                    illegalArgumentException.getMessage());
        }

    }

    /**
     * Method to test the listStories method.
     */
    @Test
    public void testListStories(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Assert that creating a list of stories when there are no stories
        // returns an empty array.
        Assert.assertEquals(new ArrayList<String>(), board.listStories());
        // Add some stories to it.
        board.createStory("a", "a");
        board.createStory("b", "");
        board.createTask("b", "a", "");
        // Assert that creating a list of these stories is successful.
        Assert.assertEquals("a", board.listStories().get(0).get(0));
        // Assert that as there are no tasks in story "a" it is complete.
        Assert.assertEquals("incomplete", board.listStories().get(0).get(1));
        // Assert that as there is an incomplete task in story "b" it is
        // incomplete.
        Assert.assertEquals("incomplete", board.listStories().get(1).get(1));
        // Assert that the description for story "a" is "a".
        Assert.assertEquals("a", board.listStories().get(0).get(2));

    }

    /**
     * Method to test the delete story method.
     */
    @Test
    public void testDeleteStory(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Add some stories and tasks to the board.
        board.createStory("a", "");
        board.createStory("b", "");
        // Delete story "b".
        board.deleteStory("b");

        // Assert that story "b" doesn't exist.
        Assert.assertFalse(board.storyExists("b"));
        // Assert that story "a" still does exist.
        Assert.assertTrue(board.storyExists("a"));


    }

    /**
     * Method to test the create task method.
     */
    @Test
    public void testCreateTask(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Add a story.
        board.createStory("a", "");
        // Assert that creating a task in story "a" is successful.
        Assert.assertTrue(board.createTask("a", "a", ""));
        // Assert that creating a task in a state is successful.
        Assert.assertTrue(board.createTask("a", "d", "", "toDo"));

        // Assert that creating a task in a nonexistent story throws a story
        // does not exist exception.
        try{
            board.createTask("b", "", "");
        } catch(StoryDoesNotExistException storyDoesNotExistException){
            Assert.assertEquals("Story ID does not exist.",
                    storyDoesNotExistException.getMessage());
        }

        // Assert that creating a task with illegal arguments throws an
        // illegal arguments exception.
        try{
            board.createTask("a", "", "");
        } catch(IllegalArgumentException illegalArgumentException){
            Assert.assertEquals("Blank ID not allowed.",
                    illegalArgumentException.getMessage());
        }

        // Assert that creating a task with the same ID as another task in the
        // story throws a task already exists exception.
        try{
            board.createTask("a", "a", "");
        } catch(TaskAlreadyExistsException taskAlreadyExistsException){
            Assert.assertEquals("Task ID already exists.",
                    taskAlreadyExistsException.getMessage());
        }

        // Assert that creating a task with an invalid state throws an
        // illegal state exception.
        try{
            board.createTask("a", "b", "", "test");
        } catch(IllegalStateException illegalStateException){
            Assert.assertEquals("Invalid column.",
                    illegalStateException.getMessage());
        }

    }

    /**
     * Method to test the list tasks method.
     */
    @Test
    public void testListTasks(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();

        // Add some stories and tasks to the board.
        board.createStory("a", "");
        // Assert that creating a list of tasks when there are no tasks
        // returns an empty array.
        Assert.assertEquals(new ArrayList<String>(), board.listTasks("a"));
        board.createStory("b", "");
        board.createTask("b", "a", "a");
        board.createTask("b", "b", "");
        // Assert that creating a list of the tasks in story "b" is successful.
        Assert.assertEquals("a", board.listTasks("b").get(0).get(0));
        // Assert that task "a" in story "b" is in the ToDo state.
        Assert.assertEquals("ToDo", board.listTasks("b").get(0).get(1));
        // Assert that task "b" in story "b" is in the ToDo state.
        Assert.assertEquals("ToDo", board.listTasks("b").get(1).get(1));
        // Assert that the description for task "a" story "b" is "a".
        Assert.assertEquals("a", board.listTasks("b").get(0).get(2));

        // Assert that trying to list tasks for a nonexistent story throws the
        // story does not exist exception.
        try{
            board.listTasks("c");
        } catch(StoryDoesNotExistException storyDoesNotExistException){
            Assert.assertEquals("Story ID does not exist.",
                    storyDoesNotExistException.getMessage());
        }


    }


    /**
     * Method to test the delete task method.
     */
    @Test
    public void testDeleteTask(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Add a story.
        board.createStory("a", "");
        // Create a few tasks in story "a".
        board.createTask("a", "a", "");
        board.createTask("a", "b", "");
        // Assert that deleting task "b" is successful.
        Assert.assertTrue(board.deleteTask("a", "b"));

        // Assert that deleting a task in a nonexistent story throws a story
        // does not exist exception.
        try{
            board.deleteTask("b", "");
        } catch(StoryDoesNotExistException storyDoesNotExistException){
            Assert.assertEquals("Story ID does not exist.",
                    storyDoesNotExistException.getMessage());
        }

        // Assert that deleting a task that doesn't exist in the story throws
        // a task already exists exception.
        try{
            board.deleteTask("a", "");
        } catch(TaskDoesNotExistException taskDoesNotExissException){
            Assert.assertEquals("Task ID does not exist.",
                    taskDoesNotExissException.getMessage());
        }

    }

    /**
     * Method to test the move task method.
     */
    @Test
    public void testMoveTask(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Add a story.
        board.createStory("a", "");
        // Create a task in story "a".
        board.createTask("a", "a", "");
        // Move the task to the InProcess state.
        board.moveTask("a", "a", "in ProCess");

        // Assert that trying to make an illegal state change throws an
        // illegal state change exception.
        try{
            board.moveTask("a", "a", "done");
        } catch(IllegalStateChangeException illegalStateChangeException){
            Assert.assertEquals("Move not permitted.",
                    illegalStateChangeException.getMessage());
        }

        // Assert that trying to move to an unspecified column throws an
        // illegal state exception.
        try{
            board.moveTask("a", "a", "test");
        } catch(IllegalStateException illegalStateException){
            Assert.assertEquals("Invalid column.",
                    illegalStateException.getMessage());
        }

        // Assert that trying to move a nonexistent task throws a task does not
        // exist exception.
        try{
            board.moveTask("a", "b", "todo");
        } catch(TaskDoesNotExistException taskDoesNotExistException){
            Assert.assertEquals("Task ID does not exist.",
                    taskDoesNotExistException.getMessage());
        }

        // Assert that trying to move a task in a nonexistent story throws a
        // story does not exist exception.
        try{
            board.moveTask("b", "b", "todo");
        } catch(StoryDoesNotExistException storyDoesNotExistException){
            Assert.assertEquals("Story ID does not exist.",
                    storyDoesNotExistException.getMessage());
        }

    }

    /**
     * Method to test the updateTask method.
     */
    @Test
    public void testUpdateTask(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Add a story.
        board.createStory("a", "");
        // Create a task in story "a".
        board.createTask("a", "a", "");
        // Assert that updating the task is successful.
        Assert.assertTrue(board.updateTask("a", "a", "test"));

        // Assert that trying to update a task with an illegal description
        // throws an illegal argument exception.
        try{
            board.updateTask("a", "a", null);
        } catch(IllegalArgumentException illegalArgumentException){
            Assert.assertEquals("Null description not allowed.",
                    illegalArgumentException.getMessage());
        }

        // Assert that trying to update a nonexistent task throws a task does not
        // exist exception.
        try{
            board.updateTask("a", "b", "todo");
        } catch(TaskDoesNotExistException taskDoesNotExistException){
            Assert.assertEquals("Task ID does not exist.",
                    taskDoesNotExistException.getMessage());
        }

        // Assert that trying to update a task in a nonexistent story throws a
        // story does not exist exception.
        try{
            board.updateTask("b", "b", "todo");
        } catch(StoryDoesNotExistException storyDoesNotExistException){
            Assert.assertEquals("Story ID does not exist.",
                    storyDoesNotExistException.getMessage());
        }

    }

    /**
     * Method to test the storyExists method.
     */
    @Test
    public void testStoryExists(){

        // Create a board.
        ScrumBoard board = new ScrumBoard();
        // Assert story "a" doesn't exist.
        Assert.assertFalse(board.storyExists("a"));
        // Add a few stories.
        board.createStory("a", "");
        board.createStory("b", "");
        // Assert that these stories exist and that story "c" doesn't.
        Assert.assertTrue(board.storyExists("a"));
        Assert.assertTrue(board.storyExists("b"));
        Assert.assertFalse(board.storyExists("c"));

    }
}