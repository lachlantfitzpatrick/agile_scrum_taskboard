package scrum;

/**
 * A custom exception to handle a task not existing.
 *
 * Created by Lachlan.
 */
public class StoryDoesNotExistException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public StoryDoesNotExistException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public StoryDoesNotExistException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
