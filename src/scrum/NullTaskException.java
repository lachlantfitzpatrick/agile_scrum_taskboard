package scrum;

/**
 * A custom exception to handle null tasks.
 *
 * Created by Lachlan.
 */
public class NullTaskException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public NullTaskException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public NullTaskException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
