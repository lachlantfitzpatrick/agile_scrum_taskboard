package scrum;

/**
 * A custom exception to handle an illegal task state change.
 *
 * Created by Lachlan.
 */
public class IllegalStateChangeException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public IllegalStateChangeException() {

        // Use the inherited constructor with no string input.
        super();

        }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public IllegalStateChangeException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
