package scrum;

/**
 * The class that runs the program.
 */
public class Main {
    /**
     * The main method that runs the program.
     */
    public static void main (String args[]){

        // Initialise the controller.
        Controller controller = new Controller();

        // Try to load the previous program state.
        controller.loadProgramState();
        
        // Loop forever always looking for input.
        while(true){

            // Handle IO.
            controller.handleIO();

        }

    }
}
