package scrum;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test class for the story class.
 */
public class StoryTest {

    /**
     * Test method for testing the constructor method.
     */
    @Test
    public void testStory(){

        // Create a new story.
        Story story = new Story("a", "a");

        // Test that the null ID exception message is thrown.
        try {
            story = new Story(null, "a");
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("Null ID not allowed.", ex.getMessage());
        }

        // Test that the blank ID exception message is thrown.
        try {
            story = new Story("", "a");
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("Blank ID not allowed.", ex.getMessage());
        }

        // Test that the null description exception message is thrown.
        try {
            story = new Story("a", null);
        } catch(IllegalArgumentException ex){
            Assert.assertEquals("Null description not allowed.", ex.getMessage());
        }

    }

    /**
     * Test method for testing the createTask() method.
     */
    @Test
    public void testAddTask(){

        // Create a story.
        Story story = new Story("a", "");
        // Assert that adding a task is successful.
        Assert.assertTrue(story.createTask("a", ""));
        Assert.assertTrue(story.createTask("b", "", "toverify"));

        // Catch the exception thrown when a null task is added.
        try {
            story.createTask(null, null);
        } catch (IllegalArgumentException illegalArgumentException){
            Assert.assertEquals("Null ID not allowed.",
                    illegalArgumentException.getMessage());
        }
        // If this exception is thrown then assume the blank ID and null
        // description exceptions are too.

        // Catch the exception thrown when adding a task with duplicate ID.
        try {
            story.createTask("b", "");
        } catch (TaskAlreadyExistsException taskAlreadyExistsException){
            Assert.assertEquals("Task ID already exists.",
                    taskAlreadyExistsException.getMessage());
        }
        try {
            story.createTask("a", "");
        } catch (TaskAlreadyExistsException taskAlreadyExistsException){
            Assert.assertEquals("Task ID already exists.",
                    taskAlreadyExistsException.getMessage());
        }

    }

    /**
     * Test method for testing the deleteTask() method.
     */
    @Test
    public void testDeleteTask() {

        // Create a story.
        Story story = new Story("a", "");
        // Add a few tasks.
        story.createTask("a", "");
        story.createTask("b", "");
        // Delete task "b".
        story.deleteTask("b");

        // Assert that task "b" doesn't exist.
        Assert.assertFalse(story.taskExists("b"));
        // Assert that task "a" still does exist.
        Assert.assertTrue(story.taskExists("a"));

        // Assert that deleting a task with null ID throws a task does not
        // exist error.
        try{
            story.deleteTask(null);
        } catch(TaskDoesNotExistException taskDoesNotExistException){
            Assert.assertEquals("Task ID does not exist.",
                    taskDoesNotExistException.getMessage());
        }

    }

    /**
     * Test method for testing the moveTask method.
     */
    @Test
    public void testMoveTask(){

        // Create a story.
        Story story = new Story("a", "");
        // Add a task.
        story.createTask("a", "");

        // Assert that moving task "a" to InProcess is successful.
        Assert.assertTrue(story.moveTask("a", "inprocess"));

        // Assert that moving task "a" to Done throws an illegal state change
        // exception.
        try{
            story.moveTask("a", "done");
        } catch(IllegalStateChangeException illegalStateChangeException){
            Assert.assertEquals(illegalStateChangeException.getMessage(),
                    "Move not permitted.");
        }

    }

    /**
     * Test method for testing the updateTask method.
     */
    @Test
    public void testUpdateTask(){

        // Create a story.
        Story story = new Story("a", "");
        // Add a task.
        story.createTask("a", "");

        // Assert that updating the task description is successful.
        Assert.assertTrue(story.updateTask("a", "a"));

        // Assert that the task description is now "a".
        // Create a copy of tasks.
        ArrayList<Task> task = story.getTasks();
        // Assert that the copy of the task (and therefore the original task)
        // has the description "a".
        Assert.assertEquals("a", task.get(0).getDescription());



    }

    /**
     * Test method for testing the getTasks method. In particular focus on
     * ensuring that getTasks returns a DEEP copy of tasks.
     */
    @Test
    public void testGetTasks(){

        // Create a story.
        Story story = new Story("a", "");
        // Add a few tasks.
        story.createTask("a", "");
        story.createTask("b", "");

        // Make a deep copy of tasks.
        ArrayList<Task> tasks2 = story.getTasks();

        // Remove the first element of tasks2.
        tasks2.remove(0);

        // Assert that task "b" does not exist at the same index for both tasks.
        // Assert that task "b" is at index 0 for tasks2.
        Assert.assertEquals(tasks2.get(0).getID(), "b");
        // Assert that task "b" is at index 1 for tasks.
        Assert.assertEquals(story.getTaskIndex("b"), 1);


    }

    /**
     * Test method for testing the getTaskIndex method.
     */
    @Test
    public void testGetTaskIndex(){

        // Create a story.
        Story story = new Story("a", "");
        // Assert that adding a task is successful.
        Assert.assertTrue(story.createTask("a", ""));
        Assert.assertTrue(story.createTask("b", ""));

        // Assert that task "a" exists as the first element of tasks.
        Assert.assertEquals(story.getTaskIndex("a"), 0);
        // Assert that task "b" exists as the second element of tasks.
        Assert.assertEquals(story.getTaskIndex("b"), 1);
        // Assert that task "c" does not exist.
        try{
            story.getTaskIndex("c");
        } catch(TaskDoesNotExistException taskDoesNotExistException) {
            Assert.assertEquals("Task ID does not exist.",
                    taskDoesNotExistException.getMessage());
        }

    }

    /**
     * Test method for testing the taskExists method.
     */
    @Test
    public void testTaskExists(){

        // Create a story.
        Story story = new Story("a", "");
        // Assert task "a" doesn't exist.
        Assert.assertFalse(story.taskExists("a"));
        // Add a few tasks.
        story.createTask("a", "");
        story.createTask("b", "");
        // Assert that these tasks exist and that task "c" doesn't.
        Assert.assertTrue(story.taskExists("a"));
        Assert.assertTrue(story.taskExists("b"));
        Assert.assertFalse(story.taskExists("c"));

    }

    /**
     * Test method for testing the storyComplete and isStoryComplete methods.
     */
    @Test
    public void testStoryComplete(){

        // Create a story.
        Story story = new Story("a", "");
        // Assert that an empty story is complete.
        Assert.assertFalse(story.isStoryComplete());
        // Assert that the string representation of the state is "complete".
        Assert.assertEquals("incomplete", story.getState());

        // Add a few tasks in the Done state.
        story.createTask("a", "", "done");
        story.createTask("b", "", "done");
        // Complete the story.
        story.setState();
        // Assert that the story is complete.
        Assert.assertTrue(story.isStoryComplete());

        // Add another task in the ToVerify state.
        story.createTask("c", "", "toverify");
        // Assert that the story is not complete.
        Assert.assertFalse(story.isStoryComplete());
        // Assert that the string representation of the state is "incomplete".
        Assert.assertEquals("incomplete", story.getState());

    }

}