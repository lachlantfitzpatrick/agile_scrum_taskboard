package scrum;

/**
 * This class describes a general Scrum board item (eg. stories and items) that
 * has and ID and description. This class is used to define the ID and
 * description properties and methods.
 *
 * Created by Lachlan.
 */
public class BoardItem {

    /*
     * invariant:
     *
     * itemID != (null || "")
     *
     * && itemDescription != null
     *
     */

    // Create a string to hold the item ID.
    private String itemID;
    // Create a string to hold the item description.
    private String itemDescription;

    /**
     * The constuctor method.
     *
     * @param itemID
     *      The ID of this item.
     *
     * @param itemDescription
     *      The description of this item.
     *
     * @throws IllegalArgumentException
     *      If the item ID or description inputted breaks the class invariant
     *      the IllegalArgumentException is thrown.
     */
    public BoardItem(String itemID, String itemDescription)
            throws IllegalArgumentException{

        // Check whether the item ID or item description inputted breaks the
        // invariant.
        if (itemID == null){

            // Throw the illegal argument exception.
            throw new IllegalArgumentException("Null ID not allowed.");

        } else if(itemID.equals("")){

            // Throw the illegal argument exception.
            throw new IllegalArgumentException("Blank ID not allowed.");

        }else if(itemDescription == null){

            // Throw the illegal argument exception.
            throw new IllegalArgumentException("Null description not allowed.");

        }

        // Set the item ID.
        this.itemID = itemID;
        // Set the item description.
        this.itemDescription = itemDescription;

    }

    /**
     * Getter method that returns the item ID as a string.
     *
     * @return
     *      Returns the string representation of the item ID.
     */
    public String getID(){

        // Return the item ID.
        return itemID;

    }

    /**
     * Getter method that returns the item description as a string.
     *
     * @return
     *      Returns the string representation of the item description.
     */
    public String getDescription(){

        // Return the item ID.
        return itemDescription;

    }

    /**
     * Setter method that sets the item description.
     *
     * @param newDescription
     *      The string representation of the new description.
     *
     * @throws IllegalArgumentException
     *      If the item description inputted breaks the class invariant then
     *      an IllegalArgumentException is thrown.
     *
     * @return
     *      Returns true if the item description was successfully set.
     */
    public boolean setDescription(String newDescription)
            throws IllegalArgumentException{

        // Check whether the new description will break the invariant.
        if(newDescription == null){

            // Throw the illegal argument exception.
            throw new IllegalArgumentException("Null description not allowed.");

        } else {

            // Set the item description.
            itemDescription = newDescription;

            // Return true.
            return true;

        }
    }

}
