package scrum;

/**
 * A custom exception to handle an illegal task state.
 *
 * Created by Lachlan.
 */
public class IllegalStateException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public IllegalStateException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public IllegalStateException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
