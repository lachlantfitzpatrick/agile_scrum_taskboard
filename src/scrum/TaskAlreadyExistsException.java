package scrum;

/**
 * A custom exception to handle a duplicate task ID.
 *
 * Created by Lachlan.
 */
public class TaskAlreadyExistsException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public TaskAlreadyExistsException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public TaskAlreadyExistsException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
