package scrum;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;

/**
 * This class provides the methods to save and load the program state.
 *
 * Created by Lachlan.
 */
public class Persistence {

    // JSON object to hold the program state.
    private JSONObject programState;
    // Identifier to ensure that the file is a persistence file.
    private String magicWord = "Persistence is key!";
    // String to hold the file path.
    private String path;

    /**
     * The constructor method.
     */
    public Persistence(){

        // Initialise the JSON objects array.
        programState = new JSONObject();
        // Give it an identifier.
        programState.put("magic word", magicWord);

        // Set the file path.
        setFilePath();

    }

    /**
     * Method that takes the array lists holding the story and task data and
     * writes it to the persistence file.
     *
     * @param stories
     *      Takes an array of arrays describing all the stories in the program.
     *
     * @param tasks
     *      Takes an array of arrays describing all the tasks in the program.
     *
     * @throws IOException
     *      Throws the IOException from writeToFile().
     *
     * @return
     *      Return a true to indicate whether the method was successful.
     */
    public boolean saveProgramState(ArrayList<ArrayList<String>> stories,
                                ArrayList<ArrayList<String>> tasks)
            throws IOException{

        // Add the stories and the tasks to the JSONObject.
        addStories(stories);
        addTasks(tasks);

        // Write the JSON object to file.
        writeToFile(programState);

        // Return true.
        return true;
    }

    /**
     * Method that reads from file and returns the program state in array list
     * format. The stories and tasks can be differentiated as they have
     * different length.
     *
     * @throws IOException
     *      Throws the IOException from readFromFile().
     *
     * @throws InvalidPersistenceFileException
     *      Throws the InvalidPersistenceFileException from readFromFile().
     *
     * @return
     *      Returns an array of arrays that describe the items in the saved
     *      program state.
     */
    public ArrayList<ArrayList<String>> loadProgramState()
            throws IOException, InvalidPersistenceFileException{

        // The array list to hold the program state.
        ArrayList<ArrayList<String>> state =
                new ArrayList<ArrayList<String>>();

        // If there is a file to read from, read from the file.
        if (readFromFile()) {

            // Check whether the program state has any stories. If there are no
            // stories there can be no tasks.
            if (programState.containsKey("stories")) {

                // Extract the stories.
                JSONArray stories = (JSONArray) programState.get("stories");

                // Add each story into the state.
                for (int i = 0; i < stories.size(); i++) {

                    // Cast the story as a JSON array.
                    JSONArray story = (JSONArray) stories.get(i);

                    // Create an array list to hold the cloned story.
                    ArrayList<String> storyClone = new ArrayList<String>();

                    // Add each story characteristic to an array list.
                    for (int j = 0; j < story.size(); j++) {

                        // Add each element to the story clone.
                        storyClone.add((String) story.get(j));

                    }

                    // Add the story clone to the state.
                    state.add(storyClone);

                }

                // Check for tasks.
                if (programState.containsKey("stories")) {

                    // Extract the tasks.
                    JSONArray tasks = (JSONArray) programState.get("tasks");

                    // Add each task into the state.
                    for (int i = 0; i < tasks.size(); i++) {

                        // Cast the story as a JSON array.
                        JSONArray task = (JSONArray) tasks.get(i);

                        // Create an array list to hold the cloned task.
                        ArrayList<String> taskClone = new ArrayList<String>();

                        // Add each task characteristic to an array list.
                        for (int j = 0; j < task.size(); j++) {

                            // Add each element to the task clone.
                            taskClone.add((String) task.get(j));

                        }

                        // Add the story clone to the state.
                        state.add(taskClone);

                    }

                }

            }

        }

        // Return the state.
        return state;

    }

    /**
     * Method that adds the stories to the JSON object.
     *
     * @param stories
     *      An array of arrays that describe all the stories in the program.
     *
     * @return
     *      Returns true to indicate that the method was successful.
     */
    public boolean addStories(ArrayList<ArrayList<String>> stories){

        // Initialise a JSON array to store the stories in.
        JSONArray JSONStories = new JSONArray();

        // Put the representation of each story into the program state.
        for(ArrayList<String> story : stories){

            // Initialise a JSON array to store the story in.
            JSONArray JSONStory = new JSONArray();

            // Add each characteristic of the story to the array.
            for(String characteristic : story){

                // Add the characteristic.
                JSONStory.add(characteristic);

            }

            // Add the JSON story to JSON stories.
            JSONStories.add(JSONStory);

        }

        // Add the JSON stories to the program state.
        programState.put("stories", JSONStories);

        return true;

    }

    /**
     * Method that adds the tasks to the JSON object.
     *
     * @param tasks
     *      An array of arrays that describes all the tasks in the program.
     *
     * @return
     *      Returns true to indicate that the method was successful.
     */
    public boolean addTasks(ArrayList<ArrayList<String>> tasks){

        // Initialise a JSON array to store the tasks in.
        JSONArray JSONTasks = new JSONArray();

        // Put the representation of each task into the program state.
        for(ArrayList<String> task : tasks){

            // Initialise a JSON array to store the task in.
            JSONArray JSONStory = new JSONArray();

            // Add each characteristic of the task to the array.
            for(String characteristic : task){

                // Add the characteristic.
                JSONStory.add(characteristic);

            }

            // Add the JSON story to JSON stories.
            JSONTasks.add(JSONStory);

        }

        // Add the JSON tasks to the program state.
        programState.put("tasks", JSONTasks);

        return true;

    }

    /**
     * Method that writes the JSON object to file.
     *
     * @param programState
     *      The JSONObject that represents the current program state.
     *
     * @throws IOException
     *      Throws an IOException if an exception occurred while writing to
     *      the persistence file.
     *
     * @return
     *      Returns true to indicate that the method was successful.
     */
    public boolean writeToFile(JSONObject programState) throws IOException{

        // Create the file writer instance that relates to the file to be
        // written to.
        FileWriter saveFile = new FileWriter(path);

        // Try to write the JSON object to file.
        try{

            // Write the JSON object to the file.
            saveFile.write(programState.toJSONString());

            // Return true.
            return true;

        } catch(IOException IOException){

            // Print the exception's trace.
            IOException.printStackTrace();

            // Return false.
            return false;

        } finally {

            // Close the file.
            saveFile.flush();
            saveFile.close();

        }

    }

    /**
     * Method that sets the program state to the parsed output of the
     * persistence file.
     *
     * @throws IOException
     *      Throws an IOException if an exception occurred while writing to
     *      the persistence file.
     *
     * @throws InvalidPersistenceFileException
     *      Throws the InvalidPersistenceFileException if the persistence file
     *      is not in the correct format.
     *
     * @return
     *      Returns true if the method was successful.
     */
    public boolean readFromFile()
            throws IOException, InvalidPersistenceFileException{

        // Setup the JSON parser.
        JSONParser JSONParser = new JSONParser();

        // Try to parse the file to a JSON object.
        try{

            // Parse the file.
            Object fileObject = JSONParser.parse(new FileReader(path));

            // Cast the file object as a JSON object.
            JSONObject fileJSONObject = (JSONObject) fileObject;

            // If the file contains the correct code then we'll set out program
            // state.
            if(fileJSONObject.containsKey("magic word")){

                // Check whether the value with the key is the code.
                if(fileJSONObject.get("magic word")
                        .equals("Persistence is key!")){

                    // Check whether stories and tasks exists in the file's
                    // JSON object.
                    if(fileJSONObject.containsKey("stories") &&
                            fileJSONObject.containsKey("tasks")) {

                        // Set the program state to use the stories and tasks
                        // in the file's JSON object.
                        programState.put("stories", fileJSONObject.get("stories"));
                        programState.put("tasks", fileJSONObject.get("tasks"));

                        // Return true.
                        return true;

                    }

                }

            }

            // Otherwise throw the invalid persistence file exception.
            throw new InvalidPersistenceFileException(
                    "Invalid persistence file.");

        } catch(FileNotFoundException fileNotFoundException) {

            // Return false to indicate that no file was found.
            return false;

        } catch(ParseException parseException) {

            // Throw the invalid persistence file exception.
            throw new InvalidPersistenceFileException(
                    "Invalid persistence file.");

        } catch(InvalidPersistenceFileException invalidPersistenceFileException){

            // Throw the invalid persistence file exception again.
            throw invalidPersistenceFileException;

        } catch(Exception exception){

            // Print the exception's trace.
            exception.printStackTrace();

            // Return false.
            return false;

        }

    }

    /**
     * Method that sets the file path to the directory the program is run from.
     */
    public void setFilePath(){

        // Create the file path of the file.
        path = Main.class.getProtectionDomain().getCodeSource().
                getLocation().getPath();

        // Remove the file name off the end.
        String[] pathSplit = path.split(File.pathSeparator);
        path = "";
        int endLoop = pathSplit.length - 1;
        for(int i = 0; i < endLoop; i++){
            path += pathSplit[i] + File.pathSeparator;
        }

        // Add the filename on the end.
        path += "persistence.txt";

    }

}
