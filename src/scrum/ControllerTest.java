package scrum;

import junit.framework.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/**
 * Test for the controller class.
 */

public class ControllerTest {

    /**
     * Method to test the create story method.
     */
    @Test
    public void testCreateStory(){

        // Create an instance of the controller.
        Controller controller = new Controller();

        // Create a string to be read and an input stream to supply serial
        // input.
        String input = "update task    <  storyId >   < id  > " +
                "< new description      >      ";
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Assert that handle IO is successful.
        Assert.assertTrue(controller.handleIO());

        // Create a string to be read and an input stream to supply serial
        // input.
        input = "create story    < a  >   < id  > ";
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Assert that handle IO is successful.
        Assert.assertTrue(controller.handleIO());

        // Assert that supplying a blank story ID returns false.
        ArrayList<String> command = new ArrayList<String>();
        command.add("create story");
        command.add("");
        command.add("");
        Assert.assertFalse(controller.createStory(command));



    }

}