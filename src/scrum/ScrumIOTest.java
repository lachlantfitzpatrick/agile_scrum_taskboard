package scrum;

import junit.framework.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Test class for the ScrumIO class.
 */
public class ScrumIOTest {

    /**
     * Test method to test the read input method.
     */
    @Test
    public void testReadInput(){

        // Create an instance of the IO handler.
        ScrumIO scrumIO = new ScrumIO();

        // Create an array list to hold the formatted input.
        ArrayList<String> formattedInputComponents = new ArrayList<String>();

        // Create a string to be read and an input stream to supply serial
        // input.
        String input = "update task    <storyId><id><new description>";
        InputStream stdin = System.in;
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Read the input and assert that the components were read and
        // formatted as expected.
        formattedInputComponents = scrumIO.readInput();
        Assert.assertTrue(formattedInputComponents.get(0).
                equals("update task"));
        Assert.assertTrue(formattedInputComponents.get(1).equals("storyId"));
        Assert.assertTrue(formattedInputComponents.get(2).equals("id"));
        Assert.assertTrue(formattedInputComponents.get(3).
                equals("new description"));

    }

    /**
     * Test method to test the write output method.
     */
    @Test
    public void testWriteOutput(){

        // Create an instance of the IO handler.
        ScrumIO scrumIO = new ScrumIO();

        // Create an array list to hold the formatted input.
        ArrayList<ArrayList<String>> formattedInputComponents =
                new ArrayList<ArrayList<String>>();
        // Create an array list to hold the formatted input.
        ArrayList<String> component1 = new ArrayList<String>();
        component1.add("a");
        component1.add("b");
        // Create an array list to hold the formatted input.
        ArrayList<String> component2 = new ArrayList<String>();
        component2.add("a");
        component2.add("a");
        component2.add("a");
        // Add the string arrays to the formatted input components.
        formattedInputComponents.add(component1);
        formattedInputComponents.add(component2);

        // Assert that writing the formatted input components to serial is
        // successful.
        Assert.assertTrue(scrumIO.writeOutput(formattedInputComponents));
        // Assert that trying to write null out returns false.
        Assert.assertFalse(scrumIO.writeOutput(null));

    }

    /**
     * Test method to test the format input and output method.
     */
    @Test
    public void testFormatIO(){

        // Create an instance of the IO handler.
        ScrumIO scrumIO = new ScrumIO();

        // Create an array list to hold the formatted input.
        ArrayList<String> formattedComponents = new ArrayList<String>();

        // Format the components of a sample command and assert that the
        // components were formatted correctly.
        formattedComponents = scrumIO.
                formatInput("update task    <storyId><id><new description>");
        Assert.assertTrue(formattedComponents.get(0).
                equals("update task"));
        Assert.assertTrue(formattedComponents.get(1).equals("storyId"));
        Assert.assertTrue(formattedComponents.get(2).equals("id"));
        Assert.assertTrue(formattedComponents.get(3).
                equals("new description"));

        // Assert that formatting the components for output is correct.
        Assert.assertEquals("update task - storyId - id - new description",
                scrumIO.formatOutput(formattedComponents));
    }

}