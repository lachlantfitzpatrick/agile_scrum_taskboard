package scrum;

/**
 * This class describes the task object. This object is used to hold
 * information about a task on the Scrum 'Task Board'.
 *
 * Created by Lachlan.
 */
public class Task extends BoardItem implements Cloneable{

    /*
     * invariant:
     *
     * taskID != (null || "")
     *
     * && taskDescription != null
     *
     * && a state change cannot occur from "Done"
     *
     * && all forward state changes must be sequential (ie. ToDo to
     * InProcess to ToVerify to Done)
     *
     * && any state != Done is permitted to be moved back to ToDo or
     * InProcess
     *
     *
     */

    /**
     * An enum class to hold the different states. It is necessary for other
     * classes to create instances of this enum.
     */
    public static enum TaskState {ToDo,InProcess,ToVerify,Done}

    // Create a TaskState to hold the task state.
    private TaskState taskState;

    /**
     * The constructor method. All tasks will start in the ToDo state.
     *
     * @param taskID
     *      The ID of this task.
     *
     * @param taskDescription
     *      The description of this task.
     *
     * @throws IllegalArgumentException
     *      If the task ID or description inputted breaks the class invariant
     *      the IllegalArgumentException is thrown.
     */
    public Task(String taskID, String taskDescription)
            throws IllegalArgumentException{

        // Use the inherited constructor.
        super(taskID,taskDescription);
        // Set the task state to be ToDo.
        taskState = TaskState.ToDo;

    }

    /**
     * The constructor method. This constructor will be used for loading the
     * board state.
     *
     * @param taskID
     *      The ID of this task.
     *
     * @param taskDescription
     *      The description of this task.
     *
     * @param state
     *      The state that the task will start in.
     *
     * @throws IllegalArgumentException
     *      If the task ID or description inputted breaks the class invariant
     *      the IllegalArgumentException is thrown.
     */
    public Task(String taskID, String taskDescription, String state)
            throws IllegalArgumentException{

        // Use the inherited constructor.
        super(taskID,taskDescription);

        // Check whether the state is a legal argument.
        if(state == null){

            // Throw an illegal argument exception.
            throw new IllegalArgumentException("Null state not allowed.");
        }

        // Set the task state to be state.
        taskState = stringToTaskState(state);

    }

    /**
     * Method that returns a deep copy of Task.
     */
    public Task clone(){

        // Create a new task with the task ID description and state.
        Task task = new Task(getID(), getDescription(), getState());

        // Return the new task.
        return task;
    }

    /**
     * Getter method that returns the string representation of the task state.
     *
     * @return The string representation of the task state.
     *
     */
    public String getState(){

        // Return the string representation of the task state.
        return taskState.toString();

    }

    /**
     * Changes the task state to ToDo.
     *
     * @throws IllegalStateChangeException
     *      Throws the illegal state change exception if the current state is
     *      not legally allowed to change into the ToDo state.
     *
     * @return
     *      Return true if the state change was successful.
     *
     */
    public Boolean changeState(String proposedTaskStateString)
            throws IllegalStateChangeException{

        // Change the proposed state string to a task state.
        TaskState proposedTaskState =
                stringToTaskState(proposedTaskStateString);

        // Check the legality of the state change.
        checkStateChangeLegality(proposedTaskState);

        // Change the task state to the inputted task state.
        this.taskState = proposedTaskState;

        // The state change was successful, return true.
        return true;

    }

    /**
     * Checks whether a proposed state change is legal.
     *
     * @param proposedNewState
     *      The proposed state that a state change would change the state to.
     *
     * @throws IllegalStateChangeException
     *      Throws the illegal state change exception if the proposed state
     *      change is illegal.
     *
     * @return
     *      Return true if the state change is legal.
     *
     */
    public Boolean checkStateChangeLegality(TaskState proposedNewState)
            throws IllegalStateChangeException {

        // Switch on the proposed state.
        switch (proposedNewState){

            case ToDo:
                // Handle case for attempting to change from Done to ToDo.
                if(taskState == TaskState.Done){

                    // Throw the illegal state change exception.
                    throw new IllegalStateChangeException("Move not permitted.");

                }
                break;

            case InProcess:
                // Handle case for attempting to change from Done to InProcess.
                if(taskState == TaskState.Done){

                    // Throw the illegal state change exception.
                    throw new IllegalStateChangeException("Move not permitted.");

                }
                break;

            case ToVerify:
                // Handle cases for not attempting to move from InProcess to
                // ToVerify.
                if(taskState != TaskState.InProcess){

                    // Throw the illegal state change exception.
                    throw new IllegalStateChangeException("Move not permitted.");

                }
                break;

            case Done:
                // Handle cases for not attempting to move from ToVerify to
                // Done.
                if(taskState != TaskState.ToVerify){

                    // Throw the illegal state change exception.
                    throw new IllegalStateChangeException("Move not permitted.");

                }
                break;

        }

        return true;

    }

    /**
     * Method that takes a string representation of a state and returns the
     * TaskState representation.
     */
    public Task.TaskState stringToTaskState(String state)
            throws IllegalStateException{

        // Remove all whitespace and capital letters.
        state = state.replaceAll("\\s+", "").toLowerCase();

        // Return the appropriate state or throw an illegal state exception.
        if (state.equals("todo")){

            // Return ToDo state.
            return Task.TaskState.ToDo;

        } else if (state.equals("inprocess")){

            // Return InProcess state.
            return Task.TaskState.InProcess;

        } else if (state.equals("toverify")){

            // Return ToVerify state.
            return Task.TaskState.ToVerify;

        } else if (state.equals("done")){

            // Return Done state.
            return Task.TaskState.Done;

        } else{

            // Throw illegal state exception.
            throw new IllegalStateException("Invalid column.");

        }

    }

}
