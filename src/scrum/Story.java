package scrum;

import java.util.ArrayList;

/**
 * This class describes the story object. This object is used to hold a series
 * of tasks that are in this story.
 *
 * Created by Lachlan.
 */
public class Story extends BoardItem{

    /*
     * invariant:
     *
     * storyID != (null || "")
     *
     * && storyDescription != null
     *
     * && no tasks with the same ID
     *
     */

    // ArrayList to hold the tasks that are in this story.
    private ArrayList<Task> tasks;
    // Boolean to indicate whether the story is complete.
    boolean isComplete;

    /**
     * The constructor method. All stories start with no tasks.
     *
     * @param storyID
     *      The ID of this task.
     *
     * @param storyDescription
     *      The description of this task.
     *
     * @throws IllegalArgumentException
     *      If the task ID or description inputted breaks the class invariant
     *      the IllegalArgumentException is thrown.
     */
    public Story(String storyID, String storyDescription)
            throws IllegalArgumentException{

        // Use inherited constructor.
        super(storyID, storyDescription);
        // Initialise the tasks ArrayList.
        tasks = new ArrayList<Task>();
        // Initialise the is complete flag.
        isComplete = false;

    }

    /**
     * Adder method for adding a new task to tasks (creating a task).
     *
     * @param taskID
     *      The ID of the task to be added to tasks.
     *
     * @param taskDescription
     *      The description of the task to be added to tasks.
     *
     * @throws IllegalArgumentException
     *      Throws the illegal argument exception from the Task constructor.
     *
     * @throws TaskAlreadyExistsException
     *      Throws the task already exists exception if the task already
     *      exists.
     *
     * @return
     *      Return true if the task was added successfully.
     */
    public boolean createTask(String taskID, String taskDescription)
            throws IllegalArgumentException, TaskAlreadyExistsException {

        // Check whether the task exists.
        if(taskExists(taskID)){

            // Throw the task already exists exception.
            throw new TaskAlreadyExistsException("Task ID already exists.");

        }

        // Create the new task.
        Task task = new Task(taskID, taskDescription);
        // Add the new task to tasks.
        tasks.add(task);

        // Set the state of the story.
        setState();

        // Return true.
        return true;

    }

    /**
     * Adder method for adding a new task to tasks (creating a task).
     *
     * @param taskID
     *      The ID of the task to be added to tasks.
     *
     * @param taskDescription
     *      The description of the task to be added to tasks.
     *
     * @throws IllegalArgumentException
     *      Throws the illegal argument exception if the
     *
     * @return
     *      Return true if the task was added successfully.
     */
    public boolean createTask(String taskID, String taskDescription,
                              String state)
            throws IllegalArgumentException, TaskAlreadyExistsException {

        // Check whether the task exists.
        if(taskExists(taskID)){

            // Throw the task already exists exception.
            throw new TaskAlreadyExistsException("Task ID already exists.");

        }

        // Create the new task.
        Task task = new Task(taskID, taskDescription, state);
        // Add the new task to tasks.
        tasks.add(task);

        // If the story was already complete, recheck state.
        if(isComplete) {

            // Set the state of the story.
            setState();

        }

        // Return true.
        return true;

    }

    /**
     * Method that deletes a task with a given ID.
     *
     * @param taskID
     *      Takes the task ID that identifies the task to be deleted.
     *
     * @throws TaskDoesNotExistException
     *      Throws a task does not exist exception when the task to be deleted
     *      does not exist.
     *
     * @return
     *      Return true if the task was deleted successfully.
     */
    public boolean deleteTask(String taskID) throws TaskDoesNotExistException{

        // Remove the task.
        tasks.remove(getTaskIndex(taskID));

        // Return true.
        return true;

    }

    /**
     * Method that changes the task state (moves the task).
     *
     * @param taskID
     *      The ID of the task that is to be moved.
     *
     * @param state
     *      The state that the task is to be changed to.
     *
     * @throws TaskDoesNotExistException
     *      Throws a task does not exist exception if the task ID doesn't match
     *      any of the existing tasks.
     *
     * @throws IllegalStateChangeException
     *      Throws an illegal state change exception if the state change cannot
     *      legally be performed.
     *
     * @return
     *      Returns true if the task was successfully moved.
     */
    public boolean moveTask(String taskID, String state)
            throws TaskDoesNotExistException, IllegalStateChangeException{

        // Change the state.
        tasks.get(getTaskIndex(taskID)).changeState(state);

        // If the story was already complete, recheck state.
        if(isComplete) {

            // Set the state of the story.
            setState();

        }

        // Return true for success.
        return true;

    }

    /**
     * Method that sets the task description (updates the task).
     *
     * @param taskID
     *      The ID of the task that is to be updated.
     *
     * @param description
     *      The new description of the task.
     *
     * @throws IllegalArgumentException
     *      Throws the instance of the illegal argument exception from
     *      setDescription().
     *
     * @throws TaskDoesNotExistException
     *      Throws the instance of the task does not exception from
     *      getTaskIndex().
     *
     * @return
     *      Returns true if the description was successfully set.
     */
    public boolean updateTask(String taskID, String description)
            throws IllegalArgumentException, TaskDoesNotExistException{

        // Try to change the description.
        return tasks.get(getTaskIndex(taskID)).setDescription(description);

    }

    /**
     * Getter method to return a string representation of the state of the
     * story (ie. complete, incomplete).
     *
     * @return
     *      Returns the string representation of the state.
     */
    public String getState(){

        // If the story is complete return "complete".
        if(isComplete){

            // Return "complete".
            return "complete";

        }

        // Return "incomplete"
        return "incomplete";
    }

    /**
     * Getter method that returns a clone of the list of tasks.
     *
     * @return
     *      An ArrayList of tasks that is deep copy of this.tasks.
     */
    public ArrayList<Task> getTasks(){

        // Create a new array list of tasks.
        ArrayList<Task> tasksClone = new ArrayList<Task>();

        // Clone and add each task to the clone array list.
        for(Task t : tasks){

            tasksClone.add(t.clone());

        }

        // Return the cloned array list.
        return tasksClone;

    }

    /**
     * Getter method that returns the index of a task in tasks.
     *
     * @param taskID
     *      The ID of the task to get the index of.
     *
     * @throws TaskDoesNotExistException
     *      Throws a TaskDoesNotExistException to indicate that the task with
     *      the inputted task ID doesn't exist.
     *
     * @return
     *      The index of the task with the inputted task ID.
     */
    public int getTaskIndex(String taskID) throws TaskDoesNotExistException{

        // Find the task with the given ID.
        for(int i = 0; i < tasks.size(); i++){

            // Check for matching ID.
            if(tasks.get(i).getID().equals(taskID)){

                // Return index.
                return i;

            }

        }

        // No match was found, throws the task does not exist exception.
        throw new TaskDoesNotExistException("Task ID does not exist.");

    }

    /**
     * Getter method that returns whether a task exists or not in tasks.
     *
     * @param taskID
     *      The ID of the task to be tested fir existence.
     *
     * @return
     *      Return true if the task exists or false otherwise.
     */
    public boolean taskExists(String taskID){

        // Find the task with the given ID.
        for(Task task : tasks){

            // Check for matching ID.
            if(task.getID().equals(taskID)){

                // Return true for match.
                return true;

            }

        }

        // Return false for no match.
        return false;

    }

    /**
     * Method that tries to set the story to complete.
     *
     * @throws CompleteStoryFailedException
     *      Throws the CompleteStoryFailedException from setState.
     *
     * @throws StoryAlreadyCompleteException
     *      Throws the StoryAlreadyCompleteException from setState.
     *
     * @return
     *      Return true if the story was successfully completed.
     */
    public boolean completeStory()
            throws CompleteStoryFailedException, StoryAlreadyCompleteException{

        // If the story is already complete throw the story already complete
        // exception.
        if (isStoryComplete()){

            // Throw the story already complete exception.
            throw new StoryAlreadyCompleteException("Story already complete.");

        }
        // Try to set the state to complete and throw a complete story failed
        // exception.
        else if(!setState()){

            // Throw the exception.
            throw new CompleteStoryFailedException("Cannot complete story. " +
                    "Unfinished task exists.");

        }

        // Return false.
        return false;

    }

    /**
     * Method that checks whether all the tasks are Done (ie. whether the story
     * is complete).
     *
     * @return
     *      Returns the isComplete flag.
     */
    public boolean isStoryComplete(){

        // Return the is complete flag.
        if(isComplete){

            // Return true.
            return true;

        }

        // Return false.
        return false;

    }

    /**
     * Method that sets the state of the story.
     *
     * @return
     *      Returns true if the task was successfully set.
     */
    public boolean setState(){

        // Check tasks for a task not in the Done state.
        for(Task task : tasks){

            // Check if the task is not in the Done state.
            if(task.getState() != "Done"){

                // Set the is complete flag.
                isComplete = false;

                // Return complete flag.
                return false;

            }

        }

        // Set the is complete flag.
        isComplete = true;

        // Return complete flag.
        return true;

    }

}
