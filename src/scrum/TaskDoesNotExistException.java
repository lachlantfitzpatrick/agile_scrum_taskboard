package scrum;

/**
 * A custom exception to handle a task not existing.
 *
 * Created by Lachlan.
 */
public class TaskDoesNotExistException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public TaskDoesNotExistException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public TaskDoesNotExistException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}
