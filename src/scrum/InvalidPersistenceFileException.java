package scrum;

/**
 * A custom exception to handle an illegal command structure.
 *
 * Created by Lachlan.
 */
public class InvalidPersistenceFileException extends RuntimeException{

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public InvalidPersistenceFileException() {

        // Use the inherited constructor with no string input.
        super();

    }

    /**
     * Constructor method. Uses the inherited constructor method.
     */
    public InvalidPersistenceFileException(String s) {

        // Use the inherited constructor with a string input.
        super(s);

    }

}