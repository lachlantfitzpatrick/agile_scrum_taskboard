# Agile Scrum Taskboard #

This repository contains a command line based taskboard for teams following the Scrum strategy (as outlined [here](http://www.scrumguides.org/) by Schwaber and Sutherland).

### Use ###

|Input                  |Description |
|------------------------------------|-----------------------------|
|create story <id> <description> | Sets up a new story with the given ID and description|
|list stories | List all the stories that have been setup|
|delete story <id> | Deletes the story with the given ID |
|complete story <id> | Set the story with the given ID to be completed |
|create task <storyId> <taskId> <taskDescription>|Create a new task in the given story using the given task ID and description|
|list tasks <storyId>|List all the task in the given story|
|delete task <storyId> <taskId>|Deletes the task with given ID in the given story|
|move task <storyId> <taskId> <new column>| Move the task to the given column (eg. 'to do', 'in progress', etc)|
|update task <storyId> <taskId> <new description>|Change a task's description|

### Rules ###

Where the specification was ambiguous or didn't supply enough information the following rules were created:

* All IDs must contain no spaces.
* It is permitted to have two tasks with the same name but are in different stories.
* The file used to implement persistence will always be looked for in the directory that the program was run from.
* The “column” argument for the “move task” command does not discriminate on spaces or capitals.
* If terminated prematurely the exit code “-1” will be returned.


### Notes ###

The documentation for the program can be found in the “doc” folder of the supplied ZIP folder and the source files can be found in the “src” folder of the supplied ZIP folder. There are a series of test classes used for testing the lower level classes. These were implemented using Junit.

A “persistence.txt” file has been supplied which will mean that the program will start with a series of stories and tasks already implemented (included in the Appendix are the commands used to generate this state). To start from scratch just delete this file.